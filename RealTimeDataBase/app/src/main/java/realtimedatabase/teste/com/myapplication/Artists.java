package realtimedatabase.teste.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Artists extends AppCompatActivity {


    EditText edtMusic;
    Button btnAddMusic;
    ListView lstMusicas;
    SeekBar seekBar;
    TextView txtArtista, txtRating;

    DatabaseReference databaseTracks;

    List<Track> tracks;

    @Override
    protected void onStart() {
        super.onStart();

        databaseTracks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tracks.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Track track = postSnapshot.getValue(Track.class);
                    tracks.add(track);
                }

                TrackList trackListAdapter = new TrackList(Artists.this, tracks);
                lstMusicas.setAdapter(trackListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_artists);



        //Código responsável por pegar os dados da activity anterior
        Intent intent = getIntent();

        /*
        * this line is important
        * this time we are not getting the reference of a direct node
        * but inside the node track we are creating a new child with the artist id
        * and inside that node we will store all the tracks with unique ids
        * */
        databaseTracks = FirebaseDatabase.getInstance().getReference("tracks").child(intent.getStringExtra(MainActivity.ARTIST_ID));



        edtMusic = (EditText) findViewById(R.id.edtMusic);
        btnAddMusic = (Button) findViewById(R.id.btnAddMusic);
        lstMusicas = (ListView) findViewById(R.id.lstListaMusicas);
        seekBar = (SeekBar) findViewById(R.id.seekBarRating);
        txtArtista = (TextView) findViewById(R.id.txtArtista);
        txtRating = (TextView) findViewById(R.id.txtViewRating);

        txtArtista.setText(intent.getStringExtra(MainActivity.ARTIST_NAME));


        tracks = new ArrayList<>();


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtRating.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        btnAddMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTrack();
            }
        });


        lstMusicas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Track track = tracks.get(position);
                showUpdateDeleteDialog(track.getId(), track.getTrackName());
            }
        });


    }

    private void saveTrack(){

        String trackName = edtMusic.getText().toString().trim();
        int rating = seekBar.getProgress();
        if (!TextUtils.isEmpty(trackName)){
            String id = databaseTracks.push().getKey();
            Track track = new Track(id, trackName, rating);
            databaseTracks.child(id).setValue(track);
            Toast.makeText(this, "Track saved", Toast.LENGTH_SHORT).show();
            edtMusic.setText("");

        }else{
            Toast.makeText(this, "Please enter a track name", Toast.LENGTH_SHORT).show();
        }
    }



    private boolean updateTrack(String id, String trackName, int rating){

        Intent intent = getIntent();

        //o código abaixo delta o filho de um filho no nó de musicas - tracks
        //primeiro eu passo o id do pai, que no caso é o artista, depois o id da musica em questão

        //getting the tracks reference for the sepecied artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(intent.getStringExtra(MainActivity.ARTIST_ID)).child(id);


        //updating track

        Track track = new Track(id, trackName, rating);
        drTracks.setValue(track);
        Toast.makeText(getApplicationContext(), "Track Updated", Toast.LENGTH_SHORT).show();

        return true;
    }

    private boolean deleteTrack(String id){


        Intent intent = getIntent();

        //o código abaixo delta o filho de um filho no nó de musicas - tracks
        //primeiro eu passo o id do pai, que no caso é o artista, depois o id da musica em questão

        //getting the tracks reference for the sepecied artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(intent.getStringExtra(MainActivity.ARTIST_ID)).child(id);

        //removing the track
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "Track Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private void showUpdateDeleteDialog(final String trackId, String trackName){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.upate_track_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText nomeMusica = (EditText) dialogView.findViewById(R.id.edtMusic);
        final SeekBar seekBar = (SeekBar) dialogView.findViewById(R.id.seekBarRating);
        final Button update = (Button) dialogView.findViewById(R.id.btnAtualizarMusica);
        final Button delete = (Button) dialogView.findViewById(R.id.btnDeletarMusica);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtRating.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        dialogBuilder.setTitle(trackName);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nomeMusica.getText().toString().trim();
                int rating = seekBar.getProgress();

                if (!TextUtils.isEmpty(name)){
                    updateTrack(trackId, name, rating);
                    b.dismiss();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTrack(trackId);
                b.dismiss();
            }
        });
    }
}
