package realtimedatabase.teste.com.myapplication;

/**
 * Created by eric on 14/03/18.
 */

public class Track {

    private String id;
    private String trackName;
    private int rating;

    public Track (){

    }

    public Track(String id, String trackName, int rating){
        this.trackName = trackName;
        this.rating = rating;
        this.id = id;
    }

    public String getTrackName() {
        return trackName;
    }

    public int getRating() {
        return rating;
    }

    public String getId() {
        return id;
    }
}
