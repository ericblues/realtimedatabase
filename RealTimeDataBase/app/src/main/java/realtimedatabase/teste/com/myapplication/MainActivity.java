package realtimedatabase.teste.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String ARTIST_NAME = "realtimedatabase.teste.com.myapplication.artistname";
    public static final String ARTIST_ID = "realtimedatabase.teste.com.myapplication.artistid";




    private EditText edtNomeArtista;
    private Spinner spnEstiloMusical;
    private ListView lstListaArtistas;
    private Button btnAdicionar;
    private TextView txtUsuario;
    private Button btnSair;


    //firebase auth object
    private FirebaseAuth firebaseAuth;

    List<Artist> artists;

    DatabaseReference databaseArtists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //initializing firebase authentication object
        firebaseAuth = FirebaseAuth.getInstance();

        //if the user is not logged in
        //that means current user will return null
        if(firebaseAuth.getCurrentUser() == null){
            //closing this activity
            finish();
            //starting login activity
            startActivity(new Intent(this, Entrar.class));
        }

        //getting current user
        FirebaseUser user = firebaseAuth.getCurrentUser();



        //getting the reference of artists node
        databaseArtists = FirebaseDatabase.getInstance().getReference("artists");



        edtNomeArtista = (EditText) findViewById(R.id.edtNomeArtista);
        spnEstiloMusical = (Spinner) findViewById(R.id.spnEstiloMusical);
        lstListaArtistas = (ListView) findViewById(R.id.lstListaArtistas);
        btnAdicionar = (Button) findViewById(R.id.btnAdicionar);
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        btnSair = (Button) findViewById(R.id.btnSair);

        //displaying logged in user name
        txtUsuario.setText("Wellcome " + user.getEmail());

        //adding listener to button
        btnSair.setOnClickListener(this);

        //list to store artists
        artists = new ArrayList<>();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.genero));
        spnEstiloMusical.setAdapter(adapter);

        btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calling the method addArtist()
                //the method is defined below
                //this method is actually performing the write operation
                addArtist();
            }
        });

        lstListaArtistas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the selected artist
                Artist artist = artists.get(position);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), Artists.class);

                //putting artist name and id to intent
                intent.putExtra(ARTIST_ID, artist.getArtistId());
                intent.putExtra(ARTIST_NAME, artist.getArtistName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        lstListaArtistas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Artist artist = artists.get(position);
                showUpdateDeleteDialog(artist.getArtistId(), artist.getArtistName());
                return true;
            }
        });
    }


    /*
    * This method is saving a new artist to the
    * Firebase Realtime Database
    * */

    private void addArtist(){
        String name = edtNomeArtista.getText().toString().trim();
        String genre = spnEstiloMusical.getSelectedItem().toString();

        //checking if the value is provided
        if (!TextUtils.isEmpty(name)) {

            //getting a unique id using push().getKey() method
            //it will create a unique id and we will use it as the Primary Key for our Artist
            String id = databaseArtists.push().getKey();

            //creating an Artist Object
            Artist artist = new Artist(id, name, genre);

            //Saving the Artist
            databaseArtists.child(id).setValue(artist);

            //setting edittext to blank again
            edtNomeArtista.setText("");

            //displaying a success toast
            Toast.makeText(this, "Artist added", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }
    }


    private boolean updateArtist(String id, String name, String genre){
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("artists").child(id);

        //updating artist
        Artist artist = new Artist(id, name, genre);
        dR.setValue(artist);
        Toast.makeText(getApplicationContext(), "Artist Updated", Toast.LENGTH_SHORT).show();

        return true;
    }


    private void showUpdateDeleteDialog(final String artistId, String artistName){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText nomeArtista = (EditText) dialogView.findViewById(R.id.edtNomeArtista);
        final Spinner genero = (Spinner) dialogView.findViewById(R.id.spnEstiloMusical);
        final Button update = (Button) dialogView.findViewById(R.id.btnAtualizarArtista);
        final Button delete = (Button) dialogView.findViewById(R.id.btnDeletarArtista);

        dialogBuilder.setTitle(artistName);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nomeArtista.getText().toString().trim();
                String genre = genero.getSelectedItem().toString();

                if (!TextUtils.isEmpty(name)){
                    updateArtist(artistId, name, genre);
                    b.dismiss();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteArtist(artistId);
                b.dismiss();
            }
        });
    }


    private boolean deleteArtist(String id){

        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("artists").child(id);

        //removing artist
        dR.removeValue();

        //getting the tracks reference for the sepecied artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(id);

        //removing all the tracks
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "Artist Deleted", Toast.LENGTH_LONG).show();

        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();

        //attaching value event listener

        databaseArtists.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //clearing the previous artist list
                artists.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    //getting artists
                    Artist artist = postSnapshot.getValue(Artist.class);

                    //adding artist to the list
                    artists.add(artist);
                }

                //creating adapter

                ArtistList artisAdapter = new ArtistList(MainActivity.this, artists);

                //attaching adapter to the listview
                lstListaArtistas.setAdapter((artisAdapter));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btnSair){
            //logging out the user
            firebaseAuth.signOut();
            //closing activity
            finish();
            //starting login activity
            startActivity(new Intent(this, Entrar.class));
        }

    }
}
