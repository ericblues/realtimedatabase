package realtimedatabase.teste.com.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import realtimedatabase.teste.com.myapplication.Artist;
import realtimedatabase.teste.com.myapplication.R;

/**
 * Created by eric on 18/03/18.
 */

public class ArtistList extends ArrayAdapter<Artist>{

    private Activity context;
    List<Artist> artists;

    public ArtistList(Activity context, List<Artist> artists){
        super(context, R.layout.layout_artist_list, artists);
        this.context = context;
        this.artists = artists;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_artist_list, null, true);

        TextView txtName = (TextView) listViewItem.findViewById(R.id.txtViewName);
        TextView txtGenre = (TextView) listViewItem.findViewById(R.id.txtViewGenre);

        Artist artist = artists.get(position);
        txtName.setText(artist.getArtistName());
        txtGenre.setText(artist.getArtistGenre());

        return listViewItem;
    }
}
