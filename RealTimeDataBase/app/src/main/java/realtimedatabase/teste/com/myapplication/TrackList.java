package realtimedatabase.teste.com.myapplication;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by eric on 18/03/18.
 */

public class TrackList extends ArrayAdapter<Track>{

    private Activity context;
    List<Track> tracks;

    public TrackList(Activity context, List<Track> tracks){
        super(context, R.layout.layout_artist_list, tracks);
        this.context = context;
        this.tracks = tracks;
    }

    @Override
    public View getView(int position, View conterView, ViewGroup parent){

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_artist_list, null, true);

        TextView txtViewName = (TextView) listViewItem.findViewById(R.id.txtViewName);
        TextView txtViewRating = (TextView) listViewItem.findViewById(R.id.txtViewGenre);

        Track track = tracks.get(position);
        txtViewName.setText(track.getTrackName());
        txtViewRating.setText(String.valueOf(track.getRating()));

        return listViewItem;
    }
}
