package realtimedatabase.teste.com.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity implements View.OnClickListener {


    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;

    private EditText edtEmail;
    private EditText edtSenha;
    private Button btnCadastrar;
    private ProgressDialog progressDialog;
    private TextView textViewSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();


        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        textViewSignin = (TextView) findViewById(R.id.textViewSignin);


        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        btnCadastrar.setOnClickListener(this);
        textViewSignin.setOnClickListener(this);


    }

    private void registerUser(){


        String email = edtEmail.getText().toString().trim();
        String senha = edtSenha.getText().toString().trim();


        //checking if email and passwords are empty
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(senha)){
            Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();



        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, senha).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //checking if sucess
                if (task.isSuccessful()){
                    //display some message here
                    Toast.makeText(Login.this,"Successfully registered",Toast.LENGTH_LONG).show();
                }else{
                    //display some message here
                    Toast.makeText(Login.this,"Registration Error",Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        //calling register method
        if(v == btnCadastrar)
            registerUser();
        if (v == textViewSignin){
            //open login activity when user taps on the already registered textview
            startActivity(new Intent(this, Entrar.class));
        }

    }
}
